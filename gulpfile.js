var gulp = require('gulp');
var uncss = require('gulp-uncss');
var fs    = require("fs");

var concat = require('gulp-concat');

// combine all js
gulp.task('js', function() {
  return gulp.src(['./src/js/jquery.min.js', './src/js/tether.min.js', './src/js/bootstrap.min.js', './src/js/index.js'])
    .pipe(concat('./src/js/all.js'))
    .pipe(gulp.dest('./public/js/'));
});


// sass
var sass = require('gulp-sass');

gulp.task('styles', function() {
    gulp.src('src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/css/'));
});



// push to aws
var s3 = require("gulp-s3-gzip");

var _aws = JSON.parse(fs.readFileSync('./aws/aws.json'));
var options = { headers: {'Cache-Control': 'max-age=315360000, no-transform, public'} }

gulp.task('s3', function() {
  return gulp.src('./public/**')
      .pipe(s3(_aws, options));
});


//Watch task
gulp.task('default',function() {
    gulp.watch('src/scss/**/*.scss',['styles']);
});