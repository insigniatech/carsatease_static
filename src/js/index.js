$(function() {
    var headerEl = $('.site-header');
    var bodyEl = $('body');
    var viewportHeight = $(window).height();
    var headerShowHeight = headerEl.attr('data-viewport') || viewportHeight;
    var headerEnable = (headerEl.attr('data-header_enable') === 'six') ? false : true;
    var lastScrollTop = 0;
    var j = 1;

    // filter course list
    $('.srch-inpt').on('keyup', function() {
      var input, filter, ul, li, a, i;
      input = $('.srch-inpt');
      filter = input.val().toUpperCase();
      ul = $(".product-list");
      li = $('.product-list li');

      // Loop through all list items, and hide those who don't match the search query
      for (i = 0; i < li.length; i++) {
          a = li[i].getElementsByClassName("media")[0];
          if (a.innerText.toUpperCase().indexOf(filter) > -1) {
              li[i].style.display = "block";
          } else {
              li[i].style.display = "none";
          }
      }

    });


    $(window).scroll(function() {
        var scrollTop = bodyEl.scrollTop();

        /* parallax hero */
        // $('#bgvid').css('background-position-y', Math.max(-scrollTop/3, -750)+'px');
        // $('.hero-text').css('position', 'fixed');
        // if($('.hero-txt').css('opacity') == 0) {
        //   $('.hero-txt').css('visibility', 'hidden');
        // } else {
        //   $('.hero-txt').css('visibility', 'visible');
        // }
        // if (scrollTop > 150) {
        //   $('.hero-txt').css('opacity', Math.max(1-scrollTop/1000, 0));
        // } else if (scrollTop < 150) {
        //   $('.hero-txt').css('opacity', 1);
        // }

        /* header */
        if (bodyEl.hasClass('header-only-after-viewport') ||
            bodyEl.hasClass('header-initial-and-after-viewport')) {
          if (scrollTop > 150) {
            if(!headerEl.hasClass('header-trans')) {
              headerEl.addClass('header-trans');
            }
          }
          if (scrollTop > parseInt(headerShowHeight)+1 || scrollTop < 150) {
            headerEl.removeClass('header-trans');
          }
          if (scrollTop > headerShowHeight) {
            if(!headerEl.hasClass('viewport')) {
              headerEl.addClass('viewport');
            }
          } else if (scrollTop < headerShowHeight) {
            headerEl.removeClass('viewport');
          }
        } else if(bodyEl.hasClass('header-initial-scrollup')) {
          if (scrollTop > 150) {
            if(!headerEl.hasClass('header-trans')) {
              headerEl.addClass('header-trans');
            }
          }
          if (scrollTop < parseInt(lastScrollTop)+1 || scrollTop < 150) {
            headerEl.removeClass('header-trans');
          }
          if (scrollTop < 150 || scrollTop > lastScrollTop) {
            headerEl.removeClass('scrollup');
          } else if (scrollTop > 50 && scrollTop < lastScrollTop) {
            if (!headerEl.hasClass('scrollup')) {
              headerEl.addClass('scrollup');
            }
          }
          lastScrollTop = scrollTop;
        } else if(bodyEl.hasClass('header-fixed')) {
          if(scrollTop > 1) {
            if (!headerEl.hasClass('fixed')) {
              headerEl.addClass('fixed');
            }
          } else if (scrollTop < 1) {
            headerEl.removeClass('fixed');
          }
        }

    });



});
var offset = 80;

$('.navbar li a').click(function(event) {
    event.preventDefault();
    $($(this).attr('href'))[0].scrollIntoView();
    scrollBy(0, -offset);
});
